# Hernández-Garduño 2020 reproducibility

A repository for hosting all code necessary for reproducing https://doi.org/10.1016/j.orcp.2020.06.001

## `data_processing.py`

This script will pre-process COVID-19 data from https://www.gob.mx/salud/documentos/datos-abiertos-152127 

Files from 2022 are over 2Gb and were too slow to load in R.
So I have written this python script to pre-process them by removing all rows that do not match the conditions mentioned in the paper.

Usage:

``` bash
python3 data_processing.py data_file.csv year
```

Year can be either 2020, or 2022, to adjust for formatting differences.
Results will get printed to STDOUT


## `JC_data_reproduce.R`

This R script loads the pre-processed data-file and performs some OR calculations along with the logistic regression analyses.
It's still a bit rough around the edges, but it works. Patches for improving (and completing it) are welcome.
The script can be used with both data from May 2020 (like the one in the original paper) or with data from April 2022 (latest available as of writing)


## `get_data.sh`

This script takes no arguments, it just downloads the data from May 2020, and April 2022. It further contains a commented line to obtain the latest data available.


## `200515COVID19MEXICO.csv`

This is the (original) datafile from the 15th May 2020 used in the original paper.

## Data descriptors directories

Contains all the required metadata for understanding the datafiles. Note that the format changed between May 2020 and April 2022, so there are two directories contaning the respective year's metadata.
