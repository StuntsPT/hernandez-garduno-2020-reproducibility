#!/usr/bin/env python3
"""
This script will pre-process COVID-19 data from
https://www.gob.mx/salud/documentos/datos-abiertos-152127

Files from 2022 are over 2Gb and were too slow to load in R.
So I have written this python script to pre-process them by removing
all rows that do not match the conditions mentioned in the paper.

Usage: python3 data_processing.py data_file.csv year

Year can be either 2020, or 2022, to adjust for formatting differences
Results will get printed to STDOUT
"""


from sys import argv

Y2020 = {"resultf": 30, "result": ("1", "2"), "comorbs": (19, 28)}
Y2022 = {"resultf": 35, "result": ("3", "7"), "comorbs": (20, 29)}


def infile_parser(infile_path, fields):
    """
    Parses a Mexican health data file
    """
    fhandle = open(infile_path, "r", encoding="utf-8", errors="ignore")
    print(fhandle.readline(), end="")  # Skip header processing
    for lines in fhandle:
        lines = lines.strip().split(",")
        if lines[fields["resultf"]] not in fields["result"]:
            continue
        comorbs = map(int, lines[fields["comorbs"][0]:fields["comorbs"][1]])
        if sum(comorbs) != 17:
            continue
        print(",".join(lines))


if __name__ == "__main__":
    if argv[2] == "2020":
        FIELDS = Y2020
    else:
        FIELDS = Y2022
    infile_parser(argv[1], FIELDS)
