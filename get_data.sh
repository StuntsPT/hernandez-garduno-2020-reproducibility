#!/bin/bash

# Get May 2020 data
wget http://datosabiertos.salud.gob.mx/gobmx/salud/datos_abiertos/historicos/05/datos_abiertos_covid19_15.05.2020.zip

# Get April 2022 data
wget https://datosabiertos.salud.gob.mx/gobmx/salud/datos_abiertos/historicos/2022/04/datos_abiertos_covid19_19.04.2022.zip

# Get latest data
# wget https://datosabiertos.salud.gob.mx/gobmx/salud/datos_abiertos/datos_abiertos_covid19.zip
